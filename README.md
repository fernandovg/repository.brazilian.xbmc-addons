Repositório de addons brasileiros para XBMC
================================

Este repositório tem por objetivo prover addons que forneçam conteúdo
brasileiro para streaming.

*This repository is focused around providing Brazilian streaming content.*

## Addons

### Globo.tv
Vídeos do website [globo.tv][1]. Suporta também assinantes do serviço Globo+,
que fornece vídeos de programas na íntegra (novelas, jornais, seriados).

*Videos from [globo.tv][1]. Also supports Globo+ subscribers, wich provides
full episodes of original shows.*


#### Idéias

* [Muu](http://muu.globo.com/):
Serviço da Globosat de íntegras de programas dos canais de TV a Cabo da rede
(GNT, Canal Brasil, MultiShow, BIS, SporTV, Universal, Viva, entre outros).
Além de também oferecer filmes sob demanda e shows de música. Serviço
**exclusivo** para clientes NET e CTBC.

* [Crackle Brasil](http://www.crackle.com.br/):
Serviço gratuito da Sony Pictures de filmes sob demanda. Já existe um plugin
para o serviço .com, porém não funcional no Brasil.

* [PFC](http://premierefc.com/):
Streaming ao vivo de jogos do futebol brasileiro. Serviço **exclusivo**
para assinantes SKY.

* [Telecine Play](http://telecineplay.com.br/):
Filmes sob demanda da rede telecine.

## Instalação

Simplesmente faça o download do [zip do repositório][2] e o instale no XBMC.

Leia o seguinte [guia (EN)][3] no wiki oficial para instruçoes de como
instalar addons a partir de um zip.

Alternativamente, você pode usar o [plugin de instalação de repositórios][4].

*Simply download the [repository zip][2] and install it in XBMC.*

## Questões, comentários, sugestões de funcionalidades e problemas

Se você tiver qualquer um destes, vá em frente e abra um [problema][5], ou dê um alo na thread oficial do [fórum xbmc.org][6]

*If you have any of questions, comments, feature requests or issues, go ahead and submit an [issue][5] or check the official thread at [xbmc.org forum][6]



[1]: http://globotv.globo.com
[2]: https://bitbucket.org/vitorhirota/repository.brazilian.xbmc-addons/downloads/repository.brazilian.xbmc-addons-1.0.0.zip
[3]: http://wiki.xbmc.org/index.php?title=Add-ons#How_to_install_from_zip
[4]: http://passion-xbmc.org/addons/?Page=View&ID=plugin.program.repo.installer
[5]: https://bitbucket.org/vitorhirota/repository.brazilian.xbmc-addons/issues
[6]: http://forum.xbmc.org/showthread.php?tid=145951